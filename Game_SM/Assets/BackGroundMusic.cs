﻿using UnityEngine; using System.Collections;

public class BackGroundMusic : MonoBehaviour {
	public static BackGroundMusic current;
	
	AudioSource fxSound; 
	public AudioClip backMusic; // Song attached
	
	// Use this for initialization
	public void Go ()
	{
		// Audio Source responsavel por emitir os sons
		fxSound = GetComponent<AudioSource> ();
		fxSound.Play ();
	}
	
} 