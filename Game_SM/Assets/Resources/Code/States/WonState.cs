﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
{
	/// <summary>
	/// Won class
	/// </summary>
	/// <remarks>When you won here you show a feedback to the user</remarks>
	class WonState : IStateBase
	{
		/// <summary>
		/// varaible to memorize the state manager object ( core of state machine )
		/// </summary>
		private StateManager manager;   // manager game class type

		private bool save_flag = true;

		private int modality;
		private int obst_p;

		/// <summary>
		/// Create the state and load the Scene02
		/// </summary>
		/// <param name="managerRef"> store the state manager object reference</param>
		public WonState (StateManager managerRef)    // constructor
		{
			manager = managerRef;   // store a reference to StateManager class

			// Load Scene02
			if (Application.loadedLevelName != "Scene02") {
				Application.LoadLevel ("Scene02");
			}
		}

		/// <summary>
		/// Update is called by stateManager once per frame when state active
		/// </summary>
		public void StateUpdate ()
		{
			// Save results
			if (save_flag && manager.gameDataRef.playerName != "") {
				string today    = System.DateTime.Now.ToString("HH\tmm\tdd\tMM\tyyyy");
				float honey     = (float)(manager.gameDataRef.honey_collected) / (float)(manager.gameDataRef.honey_counter) * 100;
				string tot_path = Application.dataPath + manager.gameDataRef.targetpath + manager.gameDataRef.playerName + ".txt";

				// Boolean conversion from true/false to 0/1
				if(manager.gameDataRef.modality){
					modality = 1;
				} else {
					modality = 0;
				}
				if(manager.gameDataRef.obtaclePresence){
					obst_p = 1;
				} else {
					obst_p = 0;
				}

				string results  = today + "\t" + manager.gameDataRef.playerScore.ToString("0") + "\t" + 
					modality + "\t" + manager.gameDataRef.horizontal_speed + "\t" + 
						manager.gameDataRef.gameLevel + "\t" + honey.ToString("0.00") + "\t" + 
						obst_p + "\t" + manager.gameDataRef.obst_collected + "\n";

				System.IO.File.AppendAllText (tot_path, results);
				save_flag = false;
			}
		}

		/// <summary>
		/// called by stateManager onGUI once per frame when state active
		/// </summary>
		public void ShowIt ()
		{
			// Standard measures: Independent from resolution
			float std_width = 1.5f * Screen.width / 10.0f * 0.75f;
			float std_height = 1.5f * Screen.height / 10.0f * 0.75f;

			// Draw background texture
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), manager.gameDataRef.setupStateBack, ScaleMode.StretchToFill);
			GUI.DrawTexture (new Rect ((0.5f - 0.5f / 1.5f) * Screen.width, -std_height / 2f, Screen.width / 1.5f, Screen.height / 1.5f), manager.gameDataRef.winSpash);

			//Buttons
			if (GUI.Button (new Rect (Screen.width / 2 - 2.5f * std_width, Screen.height - 2.5f * std_height,
			                          std_height, std_height),
			                manager.gameDataRef.settings_Icon, manager.gameDataRef.darkstyle.label)) {
				manager.SwitchState (new SetupState (manager));
			}
			GUI.Label (new Rect (Screen.width / 2 - 2.5f * std_width - 0.75f * std_height, Screen.height - 1.5f * std_height, 
			                    2.5f * std_height, std_height), "Impostazioni", manager.gameDataRef.darkstyle.label);
			
			// Play button
			if (GUI.Button (new Rect (Screen.width / 2 - 0.5f * std_width, Screen.height - 2.5f * std_height,
			                          std_height, std_height),
			                manager.gameDataRef.replay_Icon, manager.gameDataRef.darkstyle.label)) {
				manager.gameDataRef.InitializeScore ();
				manager.SwitchState (new PlayState (manager));
			}
			GUI.Label (new Rect (Screen.width / 2 - 0.5f * std_width - 0.5f * std_height, Screen.height - 1.5f * std_height, 
			                     1.5f * std_width, std_height), "Gioca di\n nuovo", manager.gameDataRef.darkstyle.label);

			// Home button
			if (GUI.Button (new Rect (Screen.width / 2 + 1.5f * std_width, Screen.height - 2.5f * std_height,
			                          std_height, std_height),
			                manager.gameDataRef.home_Icon, manager.gameDataRef.darkstyle.label)) {
				manager.SwitchState (new BeginState (manager));
			}
			GUI.Label (new Rect (Screen.width / 2 + 1.5f * std_width - 0.5f * std_height, Screen.height - 1.5f * std_height, 
			                     1.5f * std_width, std_height), "Torna alla \n Home", manager.gameDataRef.darkstyle.label);

			std_width = Screen.width / 10.0f * 0.75f;
			std_height = Screen.height / 10.0f * 0.75f;

			// Score
			GUI.Label (new Rect (Screen.width / 2 - 4f * std_width, Screen.height * 1 / 3 + std_height, 
			                     4f * std_width, std_height), "Punteggio: ", manager.gameDataRef.darkstyle.label);
			GUI.Label (new Rect (Screen.width / 2 + std_width, Screen.height * 1 / 3 + std_height, 
			                     2f * std_width, std_height), manager.gameDataRef.playerScore.ToString ("0"), manager.gameDataRef.darkstyle.label);

			// Honey
			float honey = (float)(manager.gameDataRef.honey_collected) / (float)(manager.gameDataRef.honey_counter) * 100;
			GUI.Label (new Rect (Screen.width / 2 - 4f * std_width, Screen.height * 1 / 3 + 2f * std_height, 
			                     4f * std_width, std_height), "Miele raccolto: ", manager.gameDataRef.darkstyle.label);
			GUI.Label (new Rect (Screen.width / 2 + std_width, Screen.height * 1 / 3 + 2f * std_height, 
			                     1.5f * std_width, std_height), honey.ToString ("0") + "%", manager.gameDataRef.darkstyle.label);

			// Obstacles
			if (manager.gameDataRef.obtaclePresence) {
				float obstacles = (float)(manager.gameDataRef.obst_collected) / (float)(manager.gameDataRef.obst_counter) * 100;
				GUI.Label (new Rect (Screen.width / 2 - 4f * std_width, Screen.height * 1 / 3 + 3f * std_height, 
				                     4f * std_width, std_height), "Ostacoli presi:", manager.gameDataRef.darkstyle.label);
				GUI.Label (new Rect (Screen.width / 2 + std_width, Screen.height * 1 / 3 + 3f * std_height, 
				                     2f * std_width, std_height), manager.gameDataRef.obst_collected.ToString ("0") +
				           " ("+ obstacles.ToString("0") + "%)", manager.gameDataRef.darkstyle.label);
			}

//			GUI.Label (new Rect (Screen.width / 2 - 0.5f * std_width - 0.5f * std_height, Screen.height / 2 + 4f * std_height, 
//			                     1.5f * std_width, 3f * std_height), "Salva ed esci \n DA IMPLEMENTARE ???");

			Debug.Log ("In WonState");
		}
	}
}