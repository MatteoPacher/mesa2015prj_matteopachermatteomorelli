﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
{
    /// <summary>
    /// Instruction class.
    /// </summary>
    /// <remarks>show here the instruction for the game</remarks>
	class InstructionState: IStateBase
    {
        /// <summary>
        /// varaible to memorize the state manager object ( core of state machine )
        /// </summary>
        private StateManager manager;
		private int page = 1;

		GUIStyle style;
        /// <summary>
        /// Create the state and load the Scene03
        /// </summary>
        /// <param name="managerRef"> store the state manager object reference</param>
        public InstructionState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class

            // Load Scene03
            if (Application.loadedLevelName != "Scene03")
            {
                Application.LoadLevel("Scene03");
            }
        }

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        public void StateUpdate()
        {
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        public void ShowIt()
        {
			// Standard measures: Independent from resolution
			float std_width  = 1.5f*Screen.width / 10.0f * 0.75f;
			float std_height = 1.5f*Screen.height / 10.0f * 0.75f;

			// draw the texture store in instructionStateSplash variable that is the size of the screen
			GUI.DrawTexture (new Rect (0, 0, Screen.width, Screen.height), manager.gameDataRef.instructionStateBack, ScaleMode.StretchToFill);

			// Title
			GUI.Label (new Rect (Screen.width / 2 - 2*std_width, 0.1f * std_height, 4 * std_width, std_height),
			           "Guida", manager.gameDataRef.brightstyle.label );

			switch (page) {
			case 1:
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), manager.gameDataRef.instructionSplash01);
				GUI.Box(new Rect(Screen.width / 2 - std_width, Screen.height / 2-140, 5f*std_width, 3f*std_height), 
				        "Lo scopo del gioco è raccogliere più miele possibile.\n IL giocatore " +
				        "può muovere l'ape in alto e in basso e deve cercare di evitare gli ostacoli.",manager.gameDataRef.darkstyle.box);

//				"Lo scopo del gioco è quello di raccogliere " +
//					"la maggior quantità di miele possibile.\n Occorre fare attenzione agl" +
//						"i ostacoli però! \n Per muoversi su e giù basta usare il mouse oppure, " +
//						"se disponibile, l'input di forza.\n Premi Avanti per proseguire con il Tutorial"

				if (GUI.Button (new Rect (Screen.width - 2f*std_width, Screen.height - 2f*std_height, std_height, std_height), 
				                manager.gameDataRef.next_Icon, manager.gameDataRef.darkstyle.label) ) {
					page = page +1;
				}
				GUI.Label(new Rect (Screen.width - 2f*std_width - std_height/2, Screen.height - std_height, 2f*std_height, std_height),
				          "Avanti", manager.gameDataRef.darkstyle.label);
				break;
			case 2:
				GUI.DrawTexture(new Rect(0,0 , Screen.width, Screen.height), manager.gameDataRef.instructionSplash02);
				GUI.Box(new Rect(Screen.width / 2 - std_width, Screen.height / 2-140, 5f*std_width, 3f*std_height), 
				        "Per muovere l'ape verso l'alto basta muovere il cursore verso l'alto.\n" +
				        "Utilizzando l'input in forza basta aumentare la pressione sullo schermo o in altermativa, " +
				        "utilizzando la tastiera, premere il tasto 'B'.",manager.gameDataRef.darkstyle.box
					);

				if (GUI.Button (new Rect (Screen.width - 2f*std_width, Screen.height - 2f*std_height, std_height, std_height), 
				                manager.gameDataRef.next_Icon, manager.gameDataRef.darkstyle.label) ) {
					page = page +1;
				}
				GUI.Label(new Rect (Screen.width - 2f*std_width - std_height/2, Screen.height - std_height, 2f*std_height, std_height),
				          "Avanti", manager.gameDataRef.darkstyle.label);

				if (GUI.Button (new Rect (std_width, Screen.height - 2f*std_height, std_height, std_height), 
				                manager.gameDataRef.back_Icon, manager.gameDataRef.darkstyle.label) ) {
					page = page - 1;
				}
				GUI.Label(new Rect (std_width - std_height/2, Screen.height - std_height, 2f*std_height, std_height),
				          "Indietro", manager.gameDataRef.darkstyle.label);
				break;
			case 3:
				GUI.DrawTexture(new Rect(0,0 , Screen.width, Screen.height), manager.gameDataRef.instructionSplash03);
				GUI.Box(new Rect(Screen.width / 2 - std_width, Screen.height / 2-140, 5f*std_width, 3f*std_height),
				        "Per muovere l'ape verso il basso basta muovere il cursore verso il basso.\n" +
				        "Utilizzando l'input in forza si deve diminuire la pressione sullo schermo o in altermativa, " +
				        "utilizzando la tastiera, premere il tasto 'Z'.",manager.gameDataRef.darkstyle.box
				        );

				if (GUI.Button (new Rect (Screen.width - 2f*std_width, Screen.height - 2f*std_height, std_height, std_height), 
				                manager.gameDataRef.next_Icon, manager.gameDataRef.darkstyle.label) ) {
					page = page +1;
				}
				GUI.Label(new Rect (Screen.width - 2f*std_width - std_height/2, Screen.height - std_height, 2f*std_height, std_height),
				          "Avanti", manager.gameDataRef.darkstyle.label);

				if (GUI.Button (new Rect (std_width, Screen.height - 2f*std_height, std_height, std_height), 
				                manager.gameDataRef.back_Icon, manager.gameDataRef.darkstyle.label) ) {
					page = page - 1;
				}
				GUI.Label(new Rect (std_width - std_height/2, Screen.height - std_height, 2f*std_height, std_height),
				          "Indietro", manager.gameDataRef.darkstyle.label);
				break;
			case 4:
				GUI.DrawTexture(new Rect(0,0 , Screen.width, Screen.height), manager.gameDataRef.instructionSplash04);
				GUI.Box(new Rect(Screen.width / 2 - std_width, Screen.height / 2-140, 5f*std_width, 3f*std_height), 
				        "Il miele viene automaticamente raccolto quando il miele colpisce l'ape.",manager.gameDataRef.darkstyle.box
				        );

				if (GUI.Button (new Rect (Screen.width - 2f*std_width, Screen.height - 2f*std_height, std_height, std_height), 
				                manager.gameDataRef.next_Icon, manager.gameDataRef.darkstyle.label) ) {
					page = page +1;
				}
				GUI.Label(new Rect (Screen.width - 2f*std_width - std_height/2, Screen.height - std_height, 2f*std_height, std_height),
				          "Avanti", manager.gameDataRef.darkstyle.label);

				if (GUI.Button (new Rect (std_width, Screen.height - 2f*std_height, std_height, std_height), 
				                manager.gameDataRef.back_Icon, manager.gameDataRef.darkstyle.label) ) {
					page = page - 1;
				}
				GUI.Label(new Rect (std_width - std_height/2, Screen.height - std_height, 2f*std_height, std_height),
				          "Indietro", manager.gameDataRef.darkstyle.label);
				break;
			case 5:
				GUI.DrawTexture(new Rect(0,0 , Screen.width, Screen.height), manager.gameDataRef.instructionSplash05);
				GUI.Box(new Rect(Screen.width / 2 - std_width, Screen.height / 2-140, 5f*std_width, 3f*std_height),
				        "Cerca di evitare gli ostacoli.\n Per ogni ostacolo preso il tuo punteggio diminuirà.",
				        manager.gameDataRef.darkstyle.box);

				if (GUI.Button (new Rect (std_width, Screen.height - 2f*std_height, std_height, std_height), 
				                manager.gameDataRef.back_Icon, manager.gameDataRef.darkstyle.label) ) {
					page = page - 1;
				}
				GUI.Label(new Rect (std_width - std_height/2, Screen.height - std_height, 2f*std_height, std_height),
				          "Indietro", manager.gameDataRef.darkstyle.label);
				GUI.Box(new Rect(Screen.width/2 - std_width, Screen.height -1.7f*std_height, 2f*std_width, 1.5f*std_height),
				        "Matteo Morelli \n Matteo Pacher \n 21-07-2015",manager.gameDataRef.darkstyle.box);
				break;
			default:
				break;
			}

			if (GUI.Button (new Rect (Screen.width - std_width, Screen.height - 2f*std_height, std_height, std_height), 
			                manager.gameDataRef.home_Icon, manager.gameDataRef.darkstyle.label) || Input.GetKeyUp (KeyCode.H)) {
				// switch the state to Begin state
				manager.SwitchState (new BeginState (manager));
			}
			GUI.Label(new Rect (Screen.width - std_width, Screen.height - std_height, std_height, std_height),
			          "Home", manager.gameDataRef.darkstyle.label);


            Debug.Log("In InstructionState");
        }
    }
}