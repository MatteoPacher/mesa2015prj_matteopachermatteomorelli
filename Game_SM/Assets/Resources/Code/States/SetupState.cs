﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;
using Assets.Resources.Code.States;

namespace Assets.Resources.Code.States
{
    /// <summary>
    /// Setup Class. use this class to set the option value
    /// </summary>
    /// <remarks>TO DO: put here a detailed description of the the option vlue and how they interact with the play state</remarks>
    class SetupState : IStateBase
    {
        /// <summary>
        /// varaible to memorize the state manager object ( core of state machine )
        /// </summary>
        private StateManager manager;

		private string control_mod = "Mouse";
		private string obst_pres   = "No";
		private string levl_sel    = "Sinusoide";
		private string[] difficulty = {"Lento", "Adagio", "Medio", "Veloce"};
		private int diff_selector    = 0;

		// Flags for the dopr-down menu
		private bool showInputDown = false;
		private bool showDiffDown  = false;
		private bool showObstDown  = false;
		private bool showLevlDown  = false;

		private bool init_flag = true;

        /// <summary>
        /// Create the state and load the Scene00
        /// </summary>
        /// <param name="managerRef"> store the state manager object reference</param>
        public SetupState(StateManager managerRef)    // constructor
        {
            manager = managerRef;   // store a reference to StateManager class
			//Once the manager is loaded we must apply the lowest horizontal_speed since the "Easiest mode" is selected.
			manager.gameDataRef.horizontal_speed = manager.gameDataRef.horizontal_speed_easy;

            // Load Scene00
            if (Application.loadedLevelName != "Scene00")
            {
                Application.LoadLevel("Scene00");
            }
        }

		private void InitialiseState(){
			manager.gameDataRef.obtaclePresence = false;
		}

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        public void StateUpdate()
        {
			if (init_flag) {
				InitialiseState();
				init_flag = false;
			}

			if (manager.gameDataRef.modality == true) {
				control_mod = "Forza";
			} 
			else 
			{
				control_mod = "Mouse";
			}
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        /// <remarks>TO DO: put here a detailed description of the option value selctable and settable</remarks>
        public void ShowIt()
        {
			// Standard measures: Independent from resolution
			float std_width = Screen.width / 10.0f * 0.75f;
			float std_height = Screen.height / 10.0f * 0.75f;

			// Draw background texture
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), manager.gameDataRef.setupStateBack, ScaleMode.StretchToFill);

			// Drop-Down menu to select the input modality
			GUI.Label(new Rect(0.05f * Screen.width, Screen.height / 2 - 5f*std_height,
			                   7f * std_width, std_height), "Seleziona la modalità di input: ", manager.gameDataRef.darkstyle.box);
			// Create drop-down menu made of buttons. It appears when the user clck on the first button and dipasspears when
			// he select one option
			if (!showInputDown) {
				if(GUI.Button(new Rect(Screen.width /2 + 2f*std_width, Screen.height /2 - 5f*std_height,
				                       2f * std_width, std_height), control_mod, manager.gameDataRef.darkstyle.button)){
					showInputDown = true;
				}
			} else {
				if(GUI.Button(new Rect(Screen.width /2 + 2f*std_width, Screen.height /2 - 5f*std_height,
				                       2f * std_width, std_height), "Mouse", manager.gameDataRef.darkstyle.button)){
					manager.gameDataRef.modality = false;
					showInputDown = false;
				}
				if(GUI.Button(new Rect(Screen.width /2 + 4f*std_width, Screen.height /2 - 5f*std_height,
				                       2f * std_width, std_height), "Forza", manager.gameDataRef.darkstyle.button)){
					manager.gameDataRef.modality = true;
					showInputDown = false;
				}
			}
	        
			// Obtacles presence
			GUI.Label(new Rect(0.05f * Screen.width, Screen.height / 2 - 3.5f*std_height,
			                   7f * std_width, std_height), "Presenza di ostacoli: ", manager.gameDataRef.darkstyle.box);
			// Create drop-down menu made of buttons. It appears when the user clck on the first button and dipasspears when
			// he selects one option
			if (!showObstDown) {
				if (GUI.Button (new Rect (Screen.width / 2 + 2f * std_width, Screen.height / 2 - 3.5f*std_height,
				                          2f * std_width, std_height), obst_pres, manager.gameDataRef.darkstyle.button)) {
					showObstDown = true;
				}
			} else {
				if (GUI.Button (new Rect (Screen.width / 2 + 2f * std_width, Screen.height / 2 - 3.5f*std_height,
				                       2f * std_width, std_height), "Sì", manager.gameDataRef.darkstyle.button)) {
					manager.gameDataRef.obtaclePresence = true;
					obst_pres    = "Sì";
					showObstDown = false;
				}
				if (GUI.Button (new Rect (Screen.width / 2 + 4f * std_width, Screen.height / 2 - 3.5f * std_height,
				                       2f * std_width, std_height), "No", manager.gameDataRef.darkstyle.button)) {
					manager.gameDataRef.obtaclePresence = false;
					obst_pres    = "No";
					showObstDown = false;
				}
			}


			// Drop-Down menu to select the speed
			GUI.Label(new Rect(0.05f * Screen.width, Screen.height / 2 - 2f*std_height,
			                   7f * std_width, std_height), "Seleziona la velocità: ", manager.gameDataRef.darkstyle.box);
			// Create drop-down menu made of buttons. It appears when the user clck on the first button and dipasspears when
			// he selects one option
			if (!showDiffDown) {
				if(GUI.Button(new Rect(Screen.width /2 + 2f*std_width, Screen.height /2 - 2f*std_height,
				                       2f * std_width, std_height), difficulty[diff_selector], manager.gameDataRef.darkstyle.button)){
					showDiffDown = true;
				}
			} else {
				if(GUI.Button(new Rect(Screen.width /2 + 2f*std_width, Screen.height /2 - 2f*std_height,
				                       2f * std_width, std_height), "Lento", manager.gameDataRef.darkstyle.button)){
					diff_selector = 0;
					manager.gameDataRef.horizontal_speed = manager.gameDataRef.horizontal_speed_easy;
					showDiffDown = false;
				}
				if(GUI.Button(new Rect(Screen.width /2 + 2f*std_width, Screen.height /2 - std_height,
				                       2f * std_width, std_height), "Adagio", manager.gameDataRef.darkstyle.button)){
					diff_selector = 1;
					manager.gameDataRef.horizontal_speed = manager.gameDataRef.horizontal_speed_medium;
					//manager.gameDataRef.frequency        = manager.gameDataRef.frequency * manager.gameDataRef.horizontal_speed/manager.gameDataRef.horizontal_speed_easy;
					showDiffDown = false;
				}
				if(GUI.Button(new Rect(Screen.width /2 + 2f*std_width, Screen.height /2 ,
				                       2f * std_width, std_height), "Medio", manager.gameDataRef.darkstyle.button)){
					diff_selector = 2;
					manager.gameDataRef.horizontal_speed = manager.gameDataRef.horizontal_speed_high;
					showDiffDown = false;
				}
				if(GUI.Button(new Rect(Screen.width /2 + 2f*std_width, Screen.height /2 +std_height,
				                       2f * std_width, std_height), "Veloce", manager.gameDataRef.darkstyle.button)){
					diff_selector = 3;
					manager.gameDataRef.horizontal_speed = manager.gameDataRef.horizontal_speed_pro;
					//manager.gameDataRef.frequency        = manager.gameDataRef.frequency * manager.gameDataRef.horizontal_speed/manager.gameDataRef.horizontal_speed_easy;
					manager.gameDataRef.UpRandom         = 3;
					showDiffDown = false;
				}
			}

			// Level Selection
			// Drop-Down menu to select the Difficulty
			GUI.Label(new Rect(0.05f * Screen.width, Screen.height / 2 - 0.5f*std_height,
			                   4f * std_width, std_height), "Seleziona il livello: ", manager.gameDataRef.darkstyle.box);
			// Create drop-down menu made of buttons. It appears when the user clck on the first button and dipasspears when
			// he selects one option
			if (!showLevlDown) {
				if(GUI.Button(new Rect(0.05f * Screen.width + 4.5f * std_width, Screen.height /2 - 0.5f*std_height,
				                       2.5f*std_width, std_height), levl_sel, manager.gameDataRef.darkstyle.button)){
					showLevlDown = true;
				}
			} else {
				if(GUI.Button(new Rect(0.05f * Screen.width + 4.5f * std_width, Screen.height /2 - 0.5f*std_height,
				                       2.5f*std_width, std_height), "Sinusoide", manager.gameDataRef.darkstyle.button)){
					levl_sel = "Sinusoide";
					manager.gameDataRef.gameLevel = 1;
					showLevlDown = false;
				}
				if(GUI.Button(new Rect(0.05f * Screen.width + 4.5f * std_width, Screen.height /2 + 0.5f* std_height,
				                       2.5f*std_width, std_height), "Dente Sega", manager.gameDataRef.darkstyle.button)){
					levl_sel = "Dente Sega";
					manager.gameDataRef.gameLevel = 2;
					showLevlDown = false;
				}
				if(GUI.Button(new Rect(0.05f * Screen.width + 4.5f * std_width, Screen.height /2 + 1.5f* std_height,
				                       2.5f*std_width, std_height), "Trapezoidale", manager.gameDataRef.darkstyle.button)){
					levl_sel = "Trapezoidale";
					manager.gameDataRef.gameLevel = 3;
					showLevlDown = false;
				}
				if(GUI.Button(new Rect(0.05f * Screen.width + 4.5f * std_width, Screen.height /2 + 2.5f* std_height,
				                       2.5f*std_width, std_height), "Random", manager.gameDataRef.darkstyle.button)){
					levl_sel = "Random";
					manager.gameDataRef.gameLevel = 4;
					showLevlDown = false;
				}
			}

			// Player Name
			GUI.Label(new Rect(0.05f * Screen.width, Screen.height / 2 + 4f*std_height,
			                   3.5f * std_width, std_height), "Nome del giocatore: ", manager.gameDataRef.darkstyle.box);

			manager.gameDataRef.playerName = GUI.TextField(new Rect( 4.5f*std_width, Screen.height / 2 + 4f*std_height,4f * std_width, std_height), 
			                                               manager.gameDataRef.playerName, manager.gameDataRef.darkstyle.textField);

			// Small Icons on the top of the scene
			// Home button
			if (GUI.Button (new Rect (0.1f * std_width, 0.1f * std_height, std_height, std_height),
			                manager.gameDataRef.home_Icon, manager.gameDataRef.darkstyle.label)) {
				manager.SwitchState(new BeginState( manager ));
			}
			// Music button
			if (GUI.Button (new Rect (0.1f * std_width + 1f * std_width, 0.1f * std_height, std_height, std_height),
			                manager.gameDataRef.music_Icon, manager.gameDataRef.darkstyle.label)) {
			}
			// Close button
			if (GUI.Button (new Rect (0.1f * std_width + 2f * std_width, 0.1f * std_height, std_height, std_height),
			                manager.gameDataRef.close_Icon, manager.gameDataRef.darkstyle.label)) {
				Debug.Break ();
				Application.Quit();
				return;
			}

			// Title
			GUI.Label (new Rect (Screen.width / 2 - 2*std_width, 0.1f * std_height, 4 * std_width, std_height),
			           "Impostazioni", manager.gameDataRef.brightstyle.label );

			// Game state button
			std_height = 2 * std_height;
			std_width  = 2 * std_width;
			GUI.Label (new Rect (Screen.width - 2f*std_width/2 - std_height/2, Screen.height - std_height, 2*std_height, std_height), 
			           "Gioca", manager.gameDataRef.darkstyle.label);
			if (GUI.Button(new Rect(Screen.width - 2f*std_width/2, Screen.height - 2*std_height, std_height, std_height), 
			               manager.gameDataRef.play_Icon, manager.gameDataRef.darkstyle.label) || Input.GetKeyUp(KeyCode.I))
			{
				// switch the state to Play state and reset the score
				manager.gameDataRef.InitializeScore();
				manager.SwitchState(new PlayState(manager));
			}

//			GUI.Label (new Rect (Screen.width / 2, Screen.height / 2, 200, 200), 
//			           Application.dataPath + manager.gameDataRef.targetpath);

			Debug.Log("In SetupState");
        }
    }
}