﻿using UnityEngine;
using Assets.Resources.Code.Interfaces;

namespace Assets.Resources.Code.States
{
    /// <summary>
    /// Initial State. Usually is the initial screenshot
    /// </summary>
    public class BeginState : IStateBase
    {
        /// <summary>
        /// varaible to memorize the state manager object ( core of state machine )
        /// </summary>
        private StateManager manager;

        /// <summary>
        /// Create the state and load the Scene00
        /// </summary>
        /// <param name="managerRef"> store the state manager object reference</param>
        public BeginState(StateManager managerRef)
        {
            manager = managerRef;   // store a reference to StateManager class

            // Load Scene00
            if (Application.loadedLevelName != "Scene00")
            {
                Application.LoadLevel("Scene00");
            }
        }

        /// <summary>
        /// Update is called by stateManager once per frame when state active
        /// </summary>
        public void StateUpdate()
        {
        }

        /// <summary>
        /// called by stateManager onGUI once per frame when state active
        /// </summary>
        public void ShowIt()
        {
			// Standard measures: Independent from resolution
			float std_width = 2f*Screen.width / 10.0f * 0.75f;
			float std_height = 2f*Screen.height / 10.0f * 0.75f;

            // draw the texture store in beginStateSplash variable that is the size of the screen
			GUI.DrawTexture(new Rect(0,0 , Screen.width, Screen.height), manager.gameDataRef.beginStateBack, ScaleMode.StretchToFill);

			GUI.Box(new Rect(10, 40, 160, 60), "BETA BUILD", manager.gameDataRef.darkstyle.label);

            // Game start button
			GUI.Label (new Rect (Screen.width / 2 - 2.5f * std_width, Screen.height - std_height, std_height, 60), 
			           "Gioca", manager.gameDataRef.darkstyle.label);
			if (GUI.Button(new Rect(Screen.width / 2 - 2.5f*std_width, Screen.height - 2*std_height, std_height, std_height), 
			               manager.gameDataRef.play_Icon, manager.gameDataRef.darkstyle.label ) || Input.GetKeyUp(KeyCode.S))
            {
                // switch the state to Setup state
                manager.SwitchState(new SetupState(manager));
            }

			// Instruction state button
			GUI.Label (new Rect (Screen.width / 2 - std_width/2 - std_height/2, Screen.height - std_height, 2*std_height, 60), 
			           "Istruzioni", manager.gameDataRef.darkstyle.label);
			if (GUI.Button(new Rect(Screen.width / 2 - std_width/2, Screen.height - 2*std_height, std_height, std_height), 
			               manager.gameDataRef.instruction_Icon, manager.gameDataRef.darkstyle.label) || Input.GetKeyUp(KeyCode.I))
			{
				// switch the state to Setup state
				manager.SwitchState(new InstructionState(manager));
			}


			// Close state button
			GUI.Label (new Rect (Screen.width / 2 + 1.5f*std_width, Screen.height - std_height, std_height, 60), 
			           "Chiudi", manager.gameDataRef.darkstyle.label);
			if (GUI.Button(new Rect(Screen.width / 2 + 1.5f*std_width, Screen.height - 2*std_height, std_height, std_height),
			               manager.gameDataRef.close_Icon, manager.gameDataRef.darkstyle.label) || Input.GetKeyUp(KeyCode.Q) )
			{
				Debug.Break();
				Application.Quit();
				return;
				//// switch the state to Setup state
				//manager.SwitchState(new SetupState(manager));
			}

            Debug.Log("In BeginState");
        }
    }
}