﻿using UnityEngine;
using System.Collections;

public class PowerupScript : MonoBehaviour {

	ScoreManager scoreMan;
	StateManager managerRef;
	public AudioClip honey_sound;

	void OnTriggerEnter2D (Collider2D other){

		if (other.tag == "Player") {
			scoreMan   = GameObject.Find ("Main Camera").GetComponent<ScoreManager>();
			managerRef = GameObject.Find ("GameManager").GetComponent<StateManager>();
			scoreMan.IncreaseScore( managerRef );
			Destroy(this.gameObject);
			AudioSource.PlayClipAtPoint(honey_sound, new Vector3(1,1,1));
		}

	}

}
