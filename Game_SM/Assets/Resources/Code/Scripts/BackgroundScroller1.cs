using UnityEngine;
using System.Collections;

public class BackgroundScroller1 : MonoBehaviour {
	//public float speed = 0;
	public static BackgroundScroller1 current;

	float pos = 0;
	// Use this for initialization
	void Start () {

		current = this;
	
	}
	
	// Update is called once per frame
	public void Go(float speed) {
	
		pos += speed/100f;
		if (pos > 1.0f)
			pos -= 1.0f;

		GetComponent<Renderer> ().material.mainTextureOffset = new Vector2 (pos, 0);

	}
}
