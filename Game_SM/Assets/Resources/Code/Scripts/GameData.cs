﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Resources.Code.Interfaces;

namespace Assets.Resources.Code.Scripts
{
    /// <summary>
    /// class of game data
    /// </summary>
    /// <remarks>memorize here all the data used in this game</remarks>
    public class GameData : MonoBehaviour, IGameData
    {
		public GUISkin darkstyle;
		public GUISkin brightstyle;

        /// <summary>
        /// Texture of the begin state backgorund
        /// </summary>
        public Texture2D beginStateSplash;
        /// <summary>
        /// Texture of the begin state backgorund
        /// </summary>
        public Texture2D setupStateSplash;
        /// <summary>
        /// Texture of the setup state backgorund
        /// </summary>
        public Texture2D instructionStateSplash;
        /// <summary>
        /// Texture of the paly state backgorund
        /// </summary>
        public Texture2D playStateSplash;
        /// <summary>
        /// Texture of the won state backgorund
        /// </summary>
        public Texture2D wonStateSplash;
        /// <summary>
        /// Texture of the lost state backgorund
        /// </summary>
        public Texture2D lostStateSplash;
        
        /// <summary>
        /// class to manager Force input
        /// </summary>
        public ForceManager forceManager;

        /// <summary>
        /// color of the bar when under limit
        /// </summary>
        public Color lowForceColor = Color.yellow;
        /// <summary>
        /// color of the bar when out of limit
        /// </summary>
        public Color outForceColor = Color.red;
        /// <summary>
        /// color of bar when in the limit
        /// </summary>
        public Color okForceColor = Color.green;

        /// <summary>
        /// value of output pin of first Arduino button
        /// </summary>
        bool arduinoButtonA;
        /// <summary>
        /// get and set arduino first button output
        /// </summary>
        public bool ArduinoButtonA
        {
            get { return arduinoButtonA; }
            set { arduinoButtonA = value; }
        }

        /// <summary>
        /// value of output pin of second Arduino button
        /// </summary>
        bool arduinoButtonB;
        /// <summary>
        /// get and set arduino first button output
        /// </summary>
        public bool ArduinoButtonB
        {
            get { return arduinoButtonB; }
            set { arduinoButtonB = value; }
        }

		/////////////////////////////////////////////// MY TEXTURES /////////////////////////////////////////////////
		public Texture2D beginStateBack;
		public Texture2D instructionStateBack;
		public Texture2D setupStateBack;
		public Texture2D lostStateBack;
		public Texture2D wonStateBack;
		public Texture2D winSpash;
		public Texture2D instructionSplash01;
		public Texture2D instructionSplash02;
		public Texture2D instructionSplash03;
		public Texture2D instructionSplash04;
		public Texture2D instructionSplash05;



		public Texture2D buttonBack;
		//////////////////////////////////////////////// MY ICONS ///////////////////////////////////////////////////
		public Texture2D play_Icon;
		public Texture2D replay_Icon;
		public Texture2D pause_Icon;
		public Texture2D stop_Icon;
		public Texture2D home_Icon;
		public Texture2D settings_Icon;
		public Texture2D save_Icon;
		public Texture2D close_Icon;
		public Texture2D instruction_Icon;
		public Texture2D next_Icon;
		public Texture2D back_Icon;


		public Texture2D like_Icon;
		public Texture2D warning_Icon;
		public Texture2D star_Icon;
		public Texture2D music_Icon;

		//////////////////////////////////////////////// MY PARAMETERS ///////////////////////////////////////////////////
		// Game general parameters
		public bool modality                 = false; 	// Default mouse control
		public float horizontal_speed        = 0.0f;
		public float horizontal_speed_easy   = 0.1f;
		public float horizontal_speed_medium = 0.25f;
		public float horizontal_speed_high   = 0.5f;
		public float horizontal_speed_pro    = 0.6f;
		public float tot_length              = 100f;
		public int gameLevel 				 = 1;
		private float v 					 = 0f;
		private float a 					 = 0f;
		private bool rising 				 = true;

		// Force Parameters
		public float f_UpperLimit       = 800f;
		public float f_LowerLimit       = 000f;
		public float f_gain             = 5f;

		// Spawner parameters
		public float spawnTime          = 1f;			// Spawn time for obtacles and obtacles
		public float init_displacement  = 3f;			// Initial offset for powerups/obstacles
		public float delta_displacement = 4f;   	    // Target displacement for obtacles/powerups
		public float frequency          = 0.04f;
		public bool obtaclePresence     = false;		// Enabler of the spawn of obtacle
		public int UpRandom             = 5;

		// Score Parameters
		public int honey_counter        = 0;
		public int honey_collected      = 0;
		public int obst_counter         = 0;
		public int obst_collected       = 0;
		public float playerScore        = 0;
		public float score_amount       = 50.0f;
		public float score_loss         = 100.0f;

		//Save to save:
		public string playerName        = "";
		public string targetpath        = "/Resources/Data/";

		//public float[] Points;
		//Points = new float[100];
		int t_i; //index of current time step


		//////////////////////////////////////////////// MY FUNCTIONS ///////////////////////////////////////////////////

		public float VertMovSpawner( ){
			float windowSize = Camera.main.orthographicSize;

			switch (gameLevel) {
			case 1:				// Sinusoidal movement
				v = 0.8f * windowSize * Mathf.Sin (2f * Mathf.PI * frequency * Camera.main.transform.position.x);
				break;
			case 2:				// Dente di sega
				if(rising){
					v += windowSize/100f*horizontal_speed/horizontal_speed_high;
					if(v >= 0.8f*windowSize)
						rising = false;
				} else{
					v -= windowSize/100f*horizontal_speed/horizontal_speed_high;
					if(v <= -0.8f*windowSize)
						rising = true;
				}
				break;
			case 3:
				if(rising){
					a += windowSize/100f*horizontal_speed/horizontal_speed_high;
					if(a >= 1.75f*windowSize)
						rising = false;
				} else{
					a -= windowSize/100f*horizontal_speed/horizontal_speed_high;
					if(a <= -1.75f*windowSize)
						rising = true;
				}
				// v = Mathf.Max(-0.8f*windowSize, Mathf.Min (a, 0.8f*windowSize));
				if(a >= 0.8f*windowSize){
					v = 0.8f*windowSize;
				} else if(a <= -0.8f*windowSize){
					v = -0.8f*windowSize;
				} else {
					v = a;
				}
				break;
			case 4:
				float n = Random.Range(-75, 75);
				v       = n * 0.8f * windowSize / 75f;
				break;
			default:
				v = 0f;
				break;
			}

			return v;
		}

		public void AddPoint(float t, float y)
		{
			//Points [t_i, 1] = t;
			//Points [t_i, 2] = y;
			t_i +=  t_i;
			//Debug.Log("Point added {0}, {1}", t, y );


		}

		public void InitializeScore()
		{
			playerScore     = 0;
			honey_counter   = 0;
			honey_collected = 0;
			obst_counter    = 0;
			obst_collected  = 0;
		}

		////////////////////////////////////////////// UNITY FUNCTIONS ///////////////////////////////////////////////////

        /// <summary>
        /// Use this for initialization
        /// </summary>
        void Start()
        {
            //TO DO: when scene start set default value of player lives
            forceManager = new ForceManager();
			forceManager.ForceUpperLimit = f_UpperLimit ;
			forceManager.ForceLowerLimit = f_LowerLimit ;
        }

        /// <summary>
        /// Update is called once per frame
        /// </summary>
        void Update()
        {
        }

        /// <summary>
        /// Reset game parameter to initial value
        /// </summary>
        public void ResetGameData()
        {
            //TO DO: when scene Restart set default value of player lives
            forceManager.ResetForce();
        }

    }
}