﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour
{
		
	// Update is called once per frame
	public void ScoreUpdate (StateManager manager)
	{
		manager.gameDataRef.playerScore += Time.deltaTime * manager.gameDataRef.score_amount/10f;
	}
	
	// Powerups
	public void IncreaseScore ( StateManager manager )
	{
		manager.gameDataRef.playerScore     += manager.gameDataRef.score_amount;
		manager.gameDataRef.honey_collected += 1;
	}

	// Obtacles
	public void DecreaseScore ( StateManager manager )
	{
		manager.gameDataRef.playerScore    -= manager.gameDataRef.score_loss;
		manager.gameDataRef.obst_collected += 1;
	}

//	public void ScoreOnDisable(){
//		PlayerPrefs.SetInt ("Score", (int)playerScore);
//	}

//	public void ScoreOnGUI(){
//		
//		GUI.Label(new Rect(10, 10, 100, 30), "Punteggio: " + (int)(playerScore * 100) );
//
//	}
}
