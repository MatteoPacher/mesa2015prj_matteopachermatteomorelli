using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;

namespace UnitySampleAssets._2D
{
	[RequireComponent(typeof(PlatformerCharacter2D))]
	public class Platformer2DUserControl : MonoBehaviour
	{
		public void ControlUpdate (PlatformerCharacter2D character, GameObject VertBar, StateManager managerRef)
		{
			// Read the inputs.
			float h = managerRef.gameDataRef.horizontal_speed;

			// Mouse vertical position or Force control
			float v = NormPos (managerRef);

			// Background scroller
			if (h > 0f) {

				CloudScroller.current.Go ();
				BackgroundScroller1.current.Go (h);
				BackgroundScroller2.current.Go ();
				BackgroundScroller3.current.Go ();
			}	

			// Pass all parameters to the character control script.
			// character.Move(horizontal_vel, vertical_pos, if_crouched, if_jump)
			character.Move (h, v, false, false);

			// Vertical bar indicator movement
			VertBar.transform.position = new Vector2 (VertBar.transform.position.x, v);
		}

		private float NormPos (StateManager manager)
		{
			// Returns the saturated position of the mouse;
			float windowSize = 2.0f * Camera.main.orthographicSize;	// 10 in camera coordinates
			float saturation = 0.4f;
			float Pos;

			if (!manager.gameDataRef.modality) {
				Vector3 mousePos = Input.mousePosition;
				mousePos = (mousePos - new Vector3 (0.0f, Screen.height / 2.0f, 0.0f)) * (windowSize / Screen.height);
				Pos = mousePos.y;
			} else { 
				Pos = windowSize * (saturation/4f - 0.5f) + windowSize * (1f - saturation/2f) * (manager.gameDataRef.f_gain * manager.gameDataRef.forceManager.actualForce - manager.gameDataRef.forceManager.ForceLowerLimit) / (manager.gameDataRef.forceManager.ForceUpperLimit - manager.gameDataRef.forceManager.ForceLowerLimit);
				//Pos = windowSize * (saturation/4f - 0.5f) + windowSize * (1f - saturation/2f) * (manager.gameDataRef.f_gain * manager.gameDataRef.forceManager.actualForce - manager.gameDataRef.f_LowerLimit) / (manager.gameDataRef.f_UpperLimit - manager.gameDataRef.f_LowerLimit);
			}

			// Normalization
			if (Pos < - saturation * windowSize) {
				Pos = - saturation * windowSize;
			} else if (Pos > saturation * windowSize) {
				Pos = saturation * windowSize;
			}
			return Pos;
		}
	
	}

}