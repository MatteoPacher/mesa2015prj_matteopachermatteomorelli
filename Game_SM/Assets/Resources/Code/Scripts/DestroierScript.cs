﻿using UnityEngine;
using System.Collections;

public class DestroierScript : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other){

		if (other.gameObject.transform.parent) {
			Destroy (other.gameObject.transform.parent.gameObject); 
			Debug.Log("Parent objects destroyed");
		} else {
			Destroy(other.gameObject);
			Debug.Log("Simple objects destroyed");
		}
	}
}
