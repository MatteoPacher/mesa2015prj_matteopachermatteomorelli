﻿using UnityEngine;
using System.Collections;

public class SpawnScript : MonoBehaviour {

	public GameObject obj;

	public void Spawn () {
		Instantiate (obj , transform.position, Quaternion.identity);
		Debug.Log (transform.position);
	}
}
