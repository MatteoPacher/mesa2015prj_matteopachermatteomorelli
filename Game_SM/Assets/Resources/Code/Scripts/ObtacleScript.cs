﻿using UnityEngine;
using System.Collections;

public class ObtacleScript : MonoBehaviour {

	ScoreManager scoreMan;
	StateManager managerRef;
	public AudioClip error_sound;




	
	void OnTriggerEnter2D (Collider2D other){
		
		if (other.tag == "Player") {
			scoreMan   = GameObject.Find ("Main Camera").GetComponent<ScoreManager>();
			managerRef = GameObject.Find ("GameManager").GetComponent<StateManager>();
			scoreMan.DecreaseScore( managerRef );
			Destroy(this.gameObject);
			AudioSource.PlayClipAtPoint(error_sound, new Vector3(1,1,1));
			}
	}

}



