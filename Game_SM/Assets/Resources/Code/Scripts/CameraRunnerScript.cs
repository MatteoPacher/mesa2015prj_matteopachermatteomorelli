﻿using UnityEngine;
using System.Collections;

public class CameraRunnerScript : MonoBehaviour {

	public Transform player;
	public float horizontal_offset = 5;

	public void CameraUpdate () {

		transform.position = new Vector3 (player.position.x + horizontal_offset, 0, -10);
	
	}
}
