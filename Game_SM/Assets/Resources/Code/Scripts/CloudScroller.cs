﻿using UnityEngine;
using System.Collections;

public class CloudScroller : MonoBehaviour {
	public float speed = 0;
	public static CloudScroller current;

	float pos = 0;
	// Use this for initialization
	void Start () {

		current = this;
	
	}
	
	// Update is called once per frame
	public void Go() {
	
		pos += speed;
		if (pos > 1.0f)
			pos -= 1.0f;

		GetComponent<Renderer> ().material.mainTextureOffset = new Vector2 (pos, 0);

	}
}
