﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Resources.Code.Interfaces;


namespace Assets.Resources.Code.Scripts
{
    /// <summary>
    /// Force mamagement class
    /// </summary>
    /// <remarks>class to manage the logic of the force</remarks>
	public class ForceManager
	{ 
        /// <summary>
        /// max value of force out of it I have an error
        /// </summary>
        public static float FORCE_MAX = 1000;
        /// <summary>
        /// force lower limit, default 0
        /// </summary>
        float forceLowerLimit = 0;
        /// <summary>
        /// property to manage lower force limit
        /// </summary>
        public float ForceLowerLimit
        {
            get
            {
                return forceLowerLimit;
            }
            set
            {
                forceLowerLimit = value;
            }
        }
        /// <summary>
        /// force upper limit, default 800
        /// </summary>
        float forceUpperLimit = 800;
        /// <summary>
        /// property to manage upper force limit
        /// </summary>
        public float ForceUpperLimit
        {
            get
            {
                return forceUpperLimit;
            }
            set
            {
                forceUpperLimit = value;
            }
        }
        /// <summary>
        /// actual force value. hide it in the inpsector
        /// </summary>
        [HideInInspector]
        public float actualForce = 0;
        /// <summary>
        /// value of force just before the actula value
        /// </summary>
        private float prevForce = 0f; 
        /// <summary>
        /// remaining target time. need to be settend in the setup
        /// </summary>
        int remainingTimeSetted = 0; 
        /// <summary>
        /// remaining time in thershold value. hide in inspector
        /// </summary>
        [HideInInspector]
        public float remainingTime = 0;
        /// <summary>
        /// property to manage remaining time setted
        /// </summary>
        public int RemainingTimeSetted
        {
            get
            {
                return remainingTimeSetted;
            }
            set
            {
                remainingTimeSetted = value;
                remainingTime = value;
            }
        }
        /// <summary>
        /// memorizing if timer is active
        /// </summary>
        bool isTimerActive = false;
        /// <summary>
        /// return if timer is active
        /// </summary>
        public bool IsTimerActive
        {
            get
            {
                return isTimerActive;
            }
        }
        /// <summary>
        /// Enum declaration to manage in out force in threshold
        /// </summary>
        public enum InThreshold { under, inner, over };
        /// <summary>
        /// memorize if I'm in out or under force range
        /// </summary>
        InThreshold inThreshold = InThreshold.under;
        /// <summary>
        /// return if I'm uder outer or in force range
        /// </summary>
        public InThreshold InThresholdValue
        {
            get { return inThreshold; }
        }
        /// <summary>
        /// mean vlaue
        /// </summary>
        double meanValue = 0;
        /// <summary>
        /// total number of data
        /// </summary>
        int Nvalue = 0;
        /// <summary>
        /// standard deviation of force
        /// </summary>
        double stdDevForce = 0;
        /// <summary>
        /// property to get standard deviation of force
        /// </summary>
        public double StdDevForce
        {
            get
            {
                return stdDevForce;
            }
        }
        /// <summary>
        ///sum of quadratic value 
        /// </summary>
        double value2 = 0;
        /// <summary>
        /// standard deviation limit
        /// </summary>
        public double stdDevForcelimit = 2;
        /// <summary>
        /// true if stdDev less than a value
        /// </summary>
        bool inStdDevForveLimit = false;
        /// <summary>
        /// property to get standard deviation of force bool value
        /// </summary>
        public bool InStdDevForveLimit
        {
            get
            {
                return inStdDevForveLimit;
            }
        }
        
        /// <summary>
        /// constructor
        /// </summary>
        public ForceManager()
        {
            // set low threshold limit
            forceLowerLimit = 0;
            // set upper thereshold limit
            forceUpperLimit = 800;
            // set actual force to 0
            actualForce = 0;
            // set prev force value to the actual
            prevForce = actualForce;
            // set the force mean value
            meanValue = 0;
            // set number of aquisition for the mean to 0
            Nvalue = 0;
            // set the standard deviation of force to 0
            stdDevForce = 0;
            // set the power of 2 of the force to 0
            value2 = 0;
            // set that I'm not in StdDev limit
            inStdDevForveLimit = false;
            // at the beginning I'm under force limit
            inThreshold = InThreshold.under;

            // reset the timer
            ResetTimer();
        }

        /// <summary>
        /// update the force value
        /// </summary>
        public void UpdateForce(bool pressDown_tmp, bool pressUp_tmp)
        {
            // if the two key are pressed mantain actual force value
            if (pressDown_tmp && pressUp_tmp)
            {
                prevForce = actualForce;
                // veryfy if I'm in threshold
                VerifyThreshold();
            }
                // lese if I press only key to increase increase force until FORCE_MAX
            else if (pressUp_tmp)
            {
                prevForce = actualForce;
				actualForce++;
                if (actualForce > forceUpperLimit)
                {
                    Debug.LogWarning("Force out of threshold");
                }
                // give me an error if I go out of limit
                if (actualForce > FORCE_MAX)
                {
                    Debug.LogError("Force out of limit");
                    actualForce = FORCE_MAX;
                }
                // veryfy if I'm in threshold
                VerifyThreshold();
            }
            // lese if I press only key to decrease force decrase until 0
            else if (pressDown_tmp)
            {
                prevForce = actualForce;
                actualForce--;
                if (actualForce < 0) actualForce = 0;
                // veryfy if I'm in threshold
                VerifyThreshold();
            }
                // else reset al force value
            else
            {
                ResetForce();
//                Debug.LogWarning("No enough Force");
            }
        }

        /// <summary>
        /// Verify if I'm in force upper and lower limit
        /// </summary>
        void VerifyThreshold()
        {
            if (actualForce >= forceLowerLimit && actualForce <= forceUpperLimit)
            {
                inThreshold = InThreshold.inner;
                // compute the std deviation strating from 10 value to evaluate if I'm in cosntan value
                Nvalue++;
                meanValue += actualForce;
                value2 += actualForce * actualForce;
                if (Nvalue > 10)
                {
                    stdDevForce = Math.Sqrt(value2 / Nvalue - meanValue / Nvalue * meanValue / Nvalue);
                    // if i'm not in constant value reset std deviation parameter and the timer
                    if (stdDevForce > stdDevForcelimit )
                    {
                        meanValue = actualForce;
                        Nvalue = 1;
                        stdDevForce = 0;
                        value2 = actualForce * actualForce;
                        inStdDevForveLimit = false;
                        ResetTimer();
                    }
                        // else change bool value to true
                    else inStdDevForveLimit = true;
                }
                Debug.LogWarning("inthreshold" + actualForce + " " + inThreshold + " " + meanValue * meanValue  + " " + Nvalue + " " + value2/Nvalue);
            }
                // else reset timer and said me that i'm not in threshold
            else
            {
                if (actualForce < forceLowerLimit) inThreshold = InThreshold.under;
                else inThreshold = InThreshold.over;
                //Debug.LogWarning("inthreshold" + inThreshold);
                ResetTimer();
            }
        }

        /// <summary>
        /// Reset timer
        /// </summary>
        void ResetTimer()
        {
            remainingTime = 0;
            isTimerActive = false;
            meanValue = 0;
            Nvalue = 0;
            stdDevForce = 0;
            value2 = 0;
            inStdDevForveLimit = false;
        }

        /// <summary>
        /// Reset force value
        /// </summary>
        public void ResetForce()
        {
            actualForce = 0;
            prevForce = actualForce;
            meanValue = 0;
            Nvalue = 0;
            stdDevForce = 0;
            value2 = 0;
            inStdDevForveLimit = false;
            ResetTimer();
            inThreshold = InThreshold.under;
        }

        /// <summary>
        /// Start timer
        /// </summary>
        public void StartTimer()
        {
            remainingTime = remainingTimeSetted;
            isTimerActive = true;
        }

        /// <summary>
        /// update timer value
        /// </summary>
        /// <returns>true if timer is finished, alse return false</returns>
        public bool updateTimer()
        {
            remainingTime -= Time.deltaTime;
            if (remainingTime <= 0)
            {
                ResetTimer();
                return true;
            }
            else return false;
        }
	}
}
