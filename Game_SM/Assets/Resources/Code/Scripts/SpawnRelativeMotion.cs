﻿using UnityEngine;
using System.Collections;

public class SpawnRelativeMotion : MonoBehaviour {

	public GameObject obj;

//	//
//	public void InitializeSpawner(){
//
//	}

	// Update is called once per frame
	public void SpawnerUpdate ( StateManager managerRef ) {

		float tmp_x = obj.transform.position.x;
		float tmp_z = obj.transform.position.z;
		float tmp_y = managerRef.gameDataRef.VertMovSpawner (); //f fa qualcosa???
		obj.transform.position = new Vector3 (tmp_x, tmp_y, tmp_z) ;

		managerRef.gameDataRef.AddPoint (Time.time,tmp_y);
	
	}
}
