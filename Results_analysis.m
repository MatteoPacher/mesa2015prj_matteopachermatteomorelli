%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                       RESULTS ANALYSIS SCRIPT                           %
% - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - %
%                    Matteo Morelli, Matteo Pacher                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialization
close all;
clear;
clc;

% addpath(genpath(pwd));
path = fullfile(pwd,'Game_SM','Assets','Resources','Data');

machine = computer('arch');
if strcmp(machine,'maci64') == 1 ||strcmp(machine,'gnlxa64')
    separator = '/';
else
    separator = '\';
end
    
% Select Patient to analyze: Input Dialog
d     = dir(path);
rough = {d.name};
str   = {};
j     = 1;

% Find only .txt file
for i = 1:length(rough)
   tmp = strsplit(num2str(cell2mat(rough(i))),'.');
   if strcmp(num2str(cell2mat(tmp(end))),'txt') == 1
      str{j} = rough{i};                                       %#ok<SAGROW>
      j = j+1;
   end
end

set(0, 'DefaultUICOntrolFontSize', 16)
filename = listdlg('PromptString','Seleziona il paziente:',...
    'SelectionMode','single',...
    'ListString',str, 'ListSize', [320 200]);
set(0, 'DefaultUICOntrolFontSize', 10)
if isempty(filename) ~= 1
    data = load([path, separator ,str{filename}]);
else
    error('File not found.');
end

% Data decodification
[trials,n] = size(data);
hour       = data(:,1);
min        = data(:,2);
days       = data(:,3);
month      = data(:,4);
year       = data(:,5);
score      = data(:,6);
input      = data(:,7);
velocity   = data(:,8);
level      = data(:,9);
honey      = data(:,10);
obst_p     = data(:,11);
obst_c     = data(:,12);

%% Plots
 [pathstring,namestring,extension]=fileparts(str{filename});
 clc;
clf;
display('Ape & Miele: Analisi dei dati');
display('_____________');
display(['Nome giocatore:  ', namestring]);
display(['Ultima partita il:  ', num2str(days(end)),'-',num2str(month(end)),'-',num2str(year(end)),' alle ',...
    num2str(hour(end)),':',num2str(min(end))]);
display(['Numero di partite giocate: ',num2str(trials)]);
display(['Punteggio migliore: ',num2str(max(score))]);
display(['Totale miele raccolto: ',num2str(sum(honey))]);

%%%% Plot punteggio
black = 0.1*[1 1 1];
white = [1 1 1];
hFig = figure(1);
set(hFig, 'Position', [250 50 1000 1000])
hold on;
xlabel = {};
for i=1:1:trials
    if(level(i)==1)
        colorv= [1,0,0];
    elseif (level(i) == 2)
        colorv = [1,0.2,0];
    elseif (level(i) == 3)
        colorv = [1,1,0.2];
    elseif (level(i) == 4)
        colorv = [0,0.8,0];
    else
        colorv = [0,0,0];
    end
    
    if(input(i)==0)  
        subplot(3,1,1);
        hold on;
        plot(i,score(i),'o','Color',colorv,'MarkerSize', 10);
        title(namestring,'FontSize', 20);
        text(i,score(i),{[ '   v = ',num2str(velocity(i))], '   tocco'}, 'FontSize', 14);
        ylabel('Punteggio', 'FontSize', 18)
        set(gca, 'XTick',1:trials, 'XTickLabel',xlabel, 'FontSize', 14);
        ax(1) = gca;
        grid on;
        
        
        subplot(3,1,2);
        hold on;
        plot(i,honey(i),'o','Color',colorv,'MarkerSize', 10);  
        ylabel('% Miele', 'FontSize', 18)
        set(gca, 'XTick',1:trials, 'XTickLabel',xlabel, 'FontSize', 14);
        ax(2) = gca;
        grid on;
      
        subplot(3,1,3);
        hold on;
        if(obst_p(i)==1)
            plot(i,obst_c(i),'o','Color',colorv,'MarkerSize', 10);
        else
            plot(i,obst_c(i),'x','Color',white,'MarkerSize', 10);
        end
        ylabel('# Ostacoli presi', 'FontSize', 18)
        set(gca, 'XTick',1:trials, 'XTickLabel',xlabel, 'FontSize', 14);
        ax(3) = gca;
        grid on;
        
    else
         subplot(3,1,1);
        hold on;
        plot(i,score(i),'s','Color',black,'MarkerSize', 10,'MarkerFaceColor',colorv);
        text(i,score(i),{[ '   v = ',num2str(velocity(i))], '   forza'}, 'FontSize', 14);
        ylabel('Punteggio', 'FontSize', 18)
        set(gca, 'XTick',1:trials, 'XTickLabel',xlabel, 'FontSize', 14);
        ax(1) = gca;
        grid on;
        
        subplot(3,1,2);
        hold on;
        plot(i,honey(i),'s','Color',black,'MarkerSize', 10,'MarkerFaceColor',colorv);  
        ylabel('% Miele', 'FontSize', 18)
        set(gca, 'XTick',1:trials, 'XTickLabel',xlabel, 'FontSize', 14);
        ax(2) = gca;
        grid on;
      
        subplot(3,1,3);
        hold on;
        if(obst_p(i)==1)
            plot(i,obst_c(i),'s','Color',black,'MarkerSize', 10,'MarkerFaceColor',colorv);
        else
            plot(i,obst_c(i),'x','Color',white,'MarkerSize', 10,'MarkerFaceColor',colorv);   
        end
        ylabel('Ostacoli presi', 'FontSize', 18)
        set(gca, 'XTick',1:trials, 'XTickLabel',xlabel, 'FontSize', 14);
        ax(3) = gca;
        grid on;
    end
    

     xlabel{1,i} = [num2str(days(i)),'-',num2str(month(i)),'-',num2str(year(i)),' ', num2str(hour(i)),':',num2str(min(i))];;
end

linkaxes(ax,'x');




