﻿using UnityEngine;
using UnitySampleAssets.CrossPlatformInput;

namespace UnitySampleAssets._2D
{

    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D character;
        private bool jump;

        private void Awake()
        {
            character = GetComponent<PlatformerCharacter2D>();
        }

        private void Update()
        {
            if(!jump)
            // Read the jump input in Update so button presses aren't missed.
            jump = CrossPlatformInputManager.GetButtonDown("Jump");
        }

        private void FixedUpdate()
        {
            // Read the inputs.
            bool crouch = Input.GetKey(KeyCode.LeftControl);
            float h = CrossPlatformInputManager.GetAxis("Horizontal");

			// Mouse vertical position
			//float factor = 10f;
			//Vector3 mousePosition = MousePosCameraRF( factor );
			Vector3 mousePosition = NormMousePos();
			float v = mousePosition.y;

			// Pass all parameters to the character control script.
			// a) the character is always moving forward
			character.Move(h, v, crouch, jump);
            jump = false;
        }


		Vector3 NormMousePos(){
			// Returns the saturated position of the mouse;

			float windowSize = 2.0f * Camera.main.orthographicSize;
			float saturation = 0.4f;

			Vector3 mousePos = Input.mousePosition;
			mousePos = ( mousePos - new Vector3(0.0f, Screen.height / 2.0f ,0.0f) ) * (windowSize / Screen.height);

			if (mousePos.y < - saturation * windowSize) 
			{
				mousePos.y = - saturation * windowSize;
			} else if(mousePos.y > saturation * windowSize)
			{
				mousePos.y = saturation * windowSize;
			}
			return mousePos;
		}
	
	}

}